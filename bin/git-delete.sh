#!/usr/bin/env bash

# deletes file from git history, very destructive, very dangerous.

#add file to gitignore
#echo $1 >> .gitignore

#untrack file
#git rm -fr --cached $1


#dangerous part re:history
git filter-branch --force --prune-empty --index-filter "git rm -rf --cached --ignore-unmatch $1" --tag-name-filter cat -- --all

#clean and shrink
#echo "do at end, up 1 dir:"
#echo "git clone --no-hardlinks file://srv/minecraft"
