#!/usr/bin/env bash
# variable sourcing
. etc/server.conf

dropboxuploader="$droot/bin/dropbox_uploader.sh"

cd $dupload/world/

#get list of new and old backups
old_files=$($dropboxuploader list | cut -f 3 -d\ | grep .tar.gz$ | sort -u)
new_files=$(ls -c *.tar.gz | tail | sort -u)

for f in `comm -23 <(printf "%b\n" $old_files) <(printf "%b\n" $new_files)`; do
	$dropboxuploader delete $f
done

for f in `comm -13 <(printf "%b\n" $old_files) <(printf "%b\n" $new_files)`; do
	$dropboxuploader upload $f
done

cd -
