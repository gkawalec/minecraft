#!/usr/bin/env bash
#sourcing server.conf
. `dirname $_`/../etc/server.conf

#fteklitesp="tekkitlite-server-plus"
#ver=0.5.9

cd $dtsp

cp $dts/$fteklitejar .

unzip -o $fteklitejar -d tekkitlite

for zip in *.zip; do
  unzip -o "$zip" -d tekkitlite
done

cd $dtsp/tekkitlite

#rm -r META-INF
zip -0 -r $fteklitejar .
sleep 1

cd ..

mv $fteklitejar{,.bak}
mv tekkitlite/$fteklitejar . && rm -r tekkitlite/


zip -0 -r $fteklitesp\_v$ver.zip . -x "build.sh"
sleep 1


unzip -o $fteklitesp\_v$ver.zip -d $dts

mv $fteklitesp\_v$ver.zip $dupload
