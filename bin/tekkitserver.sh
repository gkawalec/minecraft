#!/usr/bin/env bash
#source server.conf
. `dirname $_`/../etc/server.conf

#tekfile="Tekkit_Lite_Server"
#teksrv="tekkit-server"
#tekcli="tekkit-client"
#tekjar="TekkitLite.jar"
#ver="0.5.9"

case "$1" in

update)

newminor=$(echo $ver | sed -e 's/\./ /g' | awk '{ printf( $1"."$2"."$3 +1 ) }')
newmajor=$(echo $ver | sed -e 's/\./ /g' | awk '{ printf( $1"."$2 +1".0" ) }')
#curl -O http://mirror.technicpack.net/Technic/servers//$ftekliteserv\_$newminor.zip
echo "new minor is $newminor"
echo "new major is $newmajor"
;;

fetch-server)
cd $dts
curl -O http://mirror.technicpack.net/Technic/servers/$npack/$ftekliteserv\_$ver.zip
rm -rv {config,coremods,mods,$fteklitejar}
unzip -qo -d $dts $ftekliteserv\_$ver.zip -x server.properties
rm -v $ftekliteserv\_$ver.zip
;;

fetch-client)
cd $dtc
curl -v -O http://206.217.207.1/Technic/mods/basemods/basemods-$npack-v$ver.zip
unzip -qo basemods-$npack-v$ver.zip
#rm basemods-$npack-v$ver.zip
;;




run)
cd $dts

#java -native -server -Xincgc -Xmx1536M -Xms512M -Xmn256M -XX:NewRatio=2 -Xrs -XX:+UseThreadPriorities -XX:CMSFullGCsBeforeCompaction=1 -XX:SoftRefLRUPolicyMSPerMB=1024 -XX:+CMSParallelRemarkEnabled -XX:+UseParNewGC -XX:+UseAdaptiveSizePolicy -XX:+DisableExplicitGC -Xnoclassgc -oss4M -ss4M -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=90 -XX:+UseConcMarkSweepGC -XX:UseSSE=4 -XX:+UseCMSCompactAtFullCollection -XX:ParallelGCThreads=2 -XX:+AggressiveOpts -jar $dts/$fteklitejar nogui.

# java -d32 -XX:PermSize=128m -XX:MaxPermSize=256m -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSParallelRemarkEnabled -XX:ParallelGCThreads=3 -XX:MaxGCPauseMillis=5 -XX:+UseAdaptiveGCBoundary -XX:-UseGCOverheadLimit -XX:+UseBiasedLocking -XX:SurvivorRatio=8 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -Xnoclassgc -XX:UseSSE=3 -XX:+UseLargePages -XX:+UseFastAccessorMethods -XX:+UseStringCache -XX:+UseCompressedStrings -XX:+OptimizeStringConcat -XX:+AggressiveOpts -jar $dts/$fteklitejar nogui


java -server -Xmx1536M -Xms1536M -XX:PermSize=128m -XX:+DisableExplicitGC -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -Xmn200M -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+AggressiveOpts -jar $dts/$fteklitejar nogui

# http://www.minecraftforum.net/topic/1541173-java-memory-and-minecraft/ for later
#old/backup
#java -Xmx1536M -Xms1024M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=4 -XX:+AggressiveOpts -jar $dts/$fteklitejar nogui

;;

download)
# get the latest backed up world file from the server and run it.
wget min.electro-h.ca/minecraft/world/`wget -q min.electro-h.ca/minecraft/world/ -O - | grep tar.gz | tail -n 1 | sed -e s/^\<.*=\"// -e s/\".*$//`
tar -xzvf *.tar.gz
rm *.tar.gz
;;

upload)
tar -czf world.`date "+%y-%m-%d-%H"`.bak.tar.gz -C $dts/ world/.

scp *.bak.tar.gz kawalec@min.electro-h.ca:~
rm *.bak.tar.gz
;;
esac

