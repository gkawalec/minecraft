#!/usr/bin/env bash

## Takes a zip file, assumes it's for technicsolder and tries to place it in the right spot in the repository tree.


repo=/srv/minecraft/pkg/mods

modname=$( basename $1 | sed -e "s/-v[0-9].*$//g" )


[[ ! -d $repo/$modname ]] && mkdir -p $repo/$modname && echo "A new mod, making a directory."

mv -v $1 $repo/$modname 
