# Configuration file
# Generated on 2/9/13 12:28 AM

####################
# _global_
####################

_global_ {
    ####################
    # guests
    ####################

    guests {
        S:parent=
        S:playersInGroup <
            _ENTRY_PLAYER_
         >
            S:prefix=§7[GUEST]
            I:priority=0
            S:suffix= 
    }

    ####################
    # players
    ####################

    players {
        S:parent=guests
        S:playersInGroup <
            nameless_noob
            mcimolini
            nulifier
            remcogito
            Terekane
            LunaSanguinarius
         >
            S:prefix=
            I:priority=0
            S:suffix=
    }

    ####################
    # root
    ####################

    root {
        S:parent=worldroot
        S:playersInGroup <
         >
            S:prefix= 
            I:priority=999
            S:suffix= 
    }

    ####################
    # worldroot
    ####################

    worldroot {
        S:parent=players
        S:playersInGroup <
         >
            S:prefix=§c[ZoneAdmin]
            I:priority=0
            S:suffix= 
    }

    ####################
    # _ladders_
    ####################

    _ladders_ {
        S:mainLadder <
            root
            worldroot
            players
            guests
         >
    }

}


