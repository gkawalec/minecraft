---- Minecraft Crash Report ----
// I let you down. Sorry :(

Time: 3/4/13 10:31 AM
Description: Exception in server tick loop

cpw.mods.fml.common.LoaderException: argo.jdom.JsonNodeDoesNotMatchPathElementsException: Failed to find an array at [0] while resolving [0] in [{"modinfoversion":2,"modlist":[{"authors":["DaftPVF","bspkrs","Lunatrius"],"credits":"Original by DaftPVF, currently maintained by bspkrs; additional credit to AtomicStryker, ScottKillen, MisterFiber","dependants":[],"dependencies":["Forge"],"description":"Allows you to chop down entire trees by breaking a single log.","logoFile":"","mcversion":"1.4.6","modid":"TreeCapitator","name":"TreeCapitator","parent":"","requiredMods":["Forge"],"screenshots":[],"updateUrl":"https://dl.dropbox.com/u/20748481/Minecraft/1.4.6/treeCapitatorForge.version","url":"http://www.minecraftforum.net/topic/1009577-","useDependencyInformation":"true","version":"Forge 1.4.6.r06"}]}].
	at cpw.mods.fml.common.Loader.identifyMods(Loader.java:326)
	at cpw.mods.fml.common.Loader.loadMods(Loader.java:470)
	at cpw.mods.fml.server.FMLServerHandler.beginServerLoading(FMLServerHandler.java:86)
	at cpw.mods.fml.common.FMLCommonHandler.onServerStart(FMLCommonHandler.java:351)
	at ho.c(DedicatedServer.java:64)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:458)
	at fy.run(SourceFile:849)
Caused by: argo.jdom.JsonNodeDoesNotMatchPathElementsException: Failed to find an array at [0] while resolving [0] in [{"modinfoversion":2,"modlist":[{"authors":["DaftPVF","bspkrs","Lunatrius"],"credits":"Original by DaftPVF, currently maintained by bspkrs; additional credit to AtomicStryker, ScottKillen, MisterFiber","dependants":[],"dependencies":["Forge"],"description":"Allows you to chop down entire trees by breaking a single log.","logoFile":"","mcversion":"1.4.6","modid":"TreeCapitator","name":"TreeCapitator","parent":"","requiredMods":["Forge"],"screenshots":[],"updateUrl":"https://dl.dropbox.com/u/20748481/Minecraft/1.4.6/treeCapitatorForge.version","url":"http://www.minecraftforum.net/topic/1009577-","useDependencyInformation":"true","version":"Forge 1.4.6.r06"}]}].
	at argo.jdom.JsonNodeDoesNotMatchPathElementsException.jsonNodeDoesNotMatchPathElementsException(JsonNodeDoesNotMatchPathElementsException.java:23)
	at argo.jdom.JsonNode.wrapExceptionsFor(JsonNode.java:345)
	at argo.jdom.JsonNode.getNode(JsonNode.java:86)
	at monoxide.forgebackup.coremod.BackupModContainer.<init>(BackupModContainer.java:46)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:57)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:525)
	at java.lang.Class.newInstance0(Class.java:372)
	at java.lang.Class.newInstance(Class.java:325)
	at cpw.mods.fml.common.Loader.identifyMods(Loader.java:321)
	... 6 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.4.7
	Operating System: FreeBSD (i386) version 9.1-RELEASE
	Java Version: 1.7.0_06, Oracle Corporation
	Java VM Version: OpenJDK Server VM (mixed mode), Oracle Corporation
	Memory: 1001103760 bytes (954 MB) / 1052770304 bytes (1004 MB) up to 1589641216 bytes (1516 MB)
	JVM Flags: 10 total; -Xmx1536M -Xms1024M -XX:PermSize=128m -XX:+DisableExplicitGC -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -Xmn200M -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+AggressiveOpts
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Suspicious classes: FML and Forge are installed
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP v7.26a FML v4.7.32.553 Minecraft Forge 6.6.1.524 7 mods loaded, 0 mods active
	mcp [Minecraft Coder Pack] (minecraft.jar) 
	FML [Forge Mod Loader] (coremods) 
	Forge [Minecraft Forge] (coremods) 
	CodeChickenCore [CodeChicken Core] (coremods) 
	FEPreLoader [Forge Essentials|PreLoader] (coremods) 
	NotEnoughItems [Not Enough Items] (coremods) 
	PowerCrystalsCore [PowerCrystals Core] (coremods) 
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'forge,fml'
	Type: Dedicated Server (map_server.txt)