#!/usr/bin/env bash

tekdir=".techniclauncher"
tekclient="tekkitlite"
ver="0.5.7"

case "$1" in

run)
# fix for users of special IM modules
unset XMODIFIERS GTK_IM_MODULE QT_IM_MODULE

#mkdir, if not tekkit launcher won't respect java flags
mkdir ~/$tekdir/rtemp

LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$JAVA_HOME/lib/amd64/:$JAVA_HOME/lib/i386/:$JAVA_HOME/jre/lib/amd64/:$JAVA_HOME/jre/lib/i386/" exec java -Xmx1024M -Xms512M -XX:PermSize=128m -XX:+DisableExplicitGC -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -Xmn200M -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+AggressiveOpts -classpath ~/$tekdir/technic-launcher.jar org.spoutcraft.launcher.Main
;;

upload)

cd ~/$tekdir
mv $tekclient $tekclient.bak
~/bin/tekkitclient.sh run
echo "done?"
read $status
echo $status
if [ ! $status = "done" ]; then
        exit
fi
cd $tekclient
zip -0 -r $tekclient\_v$ver.zip . -x stats/\* backups/\* saves/\*
scp $tekclient\_v$ver.zip kawalec@min.electro-h.ca:~
cd ..
rm -r $tekclient
mv $tekclient.bak $tekclient
;;
esac

