#!/usr/bin/env bash

#config files in ./config to keep
configs=InGameInfo.xml,powersuits-keybinds.cfg

#launcher location
launcherlocation=~/.techniclauncher

#minecraft type
mctype=tekkitlite

#temp folder name, also a backup
temp=migration


echo 'Run before Tekkit update'

[[ `pwd | grep --silent $launcherlocation/$mctype` || `cd $launcherlocation/$mctype` ]]

[[ ! -d $temp ]] && mkdir $temp

echo 'backing up'
echo './stats saves'
cp -a --parents stats/* $temp

echo './mod configs'
cp -a --parents `find mods -mindepth 1 -maxdepth 1 -type d` $temp

echo './configs/ to keep:
'
eval cp -va --parents config/{$configs} $temp


echo '
Backup done, launch tekkit and run update

'

echo -n 'Tekkit done updating? You can proceed, with y, while tekkit-client-plus is downloading. (y/n) '

read cont

[[ ! $cont == y ]] && echo 'Exiting and leaving backup files' && exit

echo 'Restoring files'
cp -va $temp/* .

echo -n '

Keep backup folder? (y/n) '

read keep

[[ $keep == y ]] && echo 'Keeping and exiting' && exit
rm -r $temp
echo 'Deleted and exiting'
