#!/usr/bin/env bash
## small script to package up mods for technic solder.

# make skeleton tree
mkdir -p skel/{bin,config,coremods,mods}

echo -n 'Built skeleton directory tree.
Place mod files, afterwards hit enter to continue.'

read cont

[[ ! $cont = '' ]] && echo 'GIGO; Exiting' && exit

# delete empty directories

find ./skel/ -type d -empty -delete


# guessing mod name and version info

zipname=$(find ./skel/ -type f -name "*.jar" -or -name "*.zip" -print | head -n 1 | sed -E -e "s,^.*[]/],," -e "s,.zip$|.jar$,," )

#echo $zipname
modname=$(echo ${zipname,,} | sed -E -e "s,[0-9].*$,," -e "s,[v_-]+$,," )
#echo $modname

vernum=$(echo ${zipname,,} | grep -o "[0-9].*$" )
#echo $vernum

pkgname=$(echo -n $modname-v$vernum )
echo "     Saving pkg as $pkgname.zip"

cd skel && zip -r $pkgname.zip * && mv $pkgname.zip ..

