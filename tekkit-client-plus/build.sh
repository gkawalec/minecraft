#!/usr/bin/env bash
#sourcing variables
. `dirname $_`/../etc/server.conf

#dtcp=~/tekkit-client-plus
#dtc=~/tekkit-client
#fteklitecp="tekkitlite-client-plus"
#ver=0.5.9

cd $dtcp/bin

cp $dtc/bin/modpack.jar .
unzip -o modpack.jar -d modpack

for zip in *.zip; do
  unzip -o "$zip" -d modpack
done

cd $dtcp/bin/modpack

zip -0 -r modpack.jar .

#filenames need to stabilize away from zip's temp names
sleep 1

cd $dtcp/bin

mv modpack.jar{,.bak}
mv {modpack/,}modpack.jar && rm -r modpack/

cd $dtcp


zip -0 -r $fteklitecp\_v$ver.zip . -x "build.sh"
sleep 1

mv $fteklitecp\_v$ver.zip $dupload
