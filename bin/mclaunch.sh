#!/usr/bin/env bash
#sourcing vars.
. `dirname $_`/../etc/server.conf

java -Xmx1024M -Xms1024M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=4 -XX:+AggressiveOpts -jar $droot/minecraft-server/minecraft_server.jar nogui.
