To install, extract this zip file into your minecraft directory. The folder structure has already been setup for you.
If you encounter any crashes or issues, please refer to HowToGetFESupport.txt.
For reference this is ForgeEssentials version 1.1.1.251 for Minecraft version 1.4.7

When updating ForgeEssentials to a newer version, then you may want to take a look at the update.sql